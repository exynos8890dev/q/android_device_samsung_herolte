PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/aosp_herolte.mk \
	$(LOCAL_DIR)/lineage_herolte.mk \
	$(LOCAL_DIR)/havoc_herolte.mk \
	$(LOCAL_DIR)/potato_herolte.mk \
	$(LOCAL_DIR)/bootleg_herolte.mk \

COMMON_LUNCH_CHOICES := \
    lineage_herolte-userdebug \
	aosp_herolte-userdebug \
	havoc_herolte-userdebug \
	potato_herolte-userdebug \
	bootleg_herolte-userdebug \
